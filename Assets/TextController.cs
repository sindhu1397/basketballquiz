﻿using System.Collections;

using System.Collections.Generic;

using UnityEngine;

public class time : MonoBehaviour
{

    public float gazeTime = 2f;

    private float timer;
    public TextMesh t;
    public Vector3 m;
    private bool gazedAt;

    void Shoot()
    {
        if (gazedAt)
        {
            timer += Time.deltaTime;

            if (timer >= gazeTime)
            {

                Hoop();
                timer = 0f;
            }

        }

    }
    public void Hoop()
    {
        var s = (TextMesh)Instantiate(t, m, Quaternion.identity);
    }

    public void pointerenter()
    {
        gazedAt = true;
    }

    public void pointerexit()
    {
        gazedAt = false;
    }
}