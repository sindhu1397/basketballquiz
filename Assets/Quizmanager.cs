﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Quizmanager : MonoBehaviour
{
    public Question[] q;
    private static List<Question> unanswered;
    private Question currentQuestion;
    public TextMesh tm;
    public TextMesh tmw;
    public Vector3 m;
    float avail = 5;

    [SerializeField]
    private TextMesh currentquestiontext;
    [SerializeField]
    private TextMesh currentquestionop1;
    [SerializeField]
    private TextMesh currentquestionop2;
    [SerializeField]
    private TextMesh currentquestionop3;
    [SerializeField]
    private TextMesh currentquestionop4;


    void Start()
    {
        if (unanswered == null || unanswered.Count == 0)
        {
            unanswered = q.ToList<Question>();
        }
        Setcurrentquestion();


    }



    private void Setcurrentquestion()
    {
        int RandomQuestionIndex = Random.Range(0, unanswered.Count);
        currentQuestion = unanswered[RandomQuestionIndex];

        currentquestiontext.GetComponent<TextMesh>().text = currentQuestion.question;
        currentquestionop1.GetComponent<TextMesh>().text = currentQuestion.op1;
        currentquestionop2.GetComponent<TextMesh>().text = currentQuestion.op2;
        currentquestionop3.GetComponent<TextMesh>().text = currentQuestion.op3;
        currentquestionop4.GetComponent<TextMesh>().text = currentQuestion.op4;

        unanswered.RemoveAt(RandomQuestionIndex);
    }


    public void Nextq()
    {
        Invoke("questionchange", 4);
        
    }
    public void questionchange()
    {
        if (avail > 0)
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        }
       
        if (avail == 0)
        {
            SceneManager.LoadScene("Scenes/start");
        }

    }
    public void hoop1()
    {
        Invoke("first", 9);
    }
    public void first()
    {
        if (currentQuestion.answer == currentQuestion.op1)
        {
            var notice = (TextMesh)Instantiate(tm, m, Quaternion.identity);
        }
        else
        {
            var wrong = (TextMesh)Instantiate(tmw, m, Quaternion.identity);
        }
    }
    public void hoop4()
    {
        Invoke("four", 9);
    }
    public void four()
    {
        if (currentQuestion.answer == currentQuestion.op4)
        {
            var notice = (TextMesh)Instantiate(tm, m, Quaternion.identity);
        }
        else
        {
            var wrong = (TextMesh)Instantiate(tmw, m, Quaternion.identity);
        }
    }
    public void hoop2()
    {
        Invoke("second", 9);
    }
    public void second()
    {
        if (currentQuestion.answer == currentQuestion.op2)
        {
            var notice = (TextMesh)Instantiate(tm, m, Quaternion.identity);
        }
        else
        {
            var wrong = (TextMesh)Instantiate(tmw, m, Quaternion.identity);
        }
    }
    public void hoop3()
    {
        Invoke("third", 9);
    }
    public void third()
    {
        if (currentQuestion.answer == currentQuestion.op3)
        {
            var notice = (TextMesh)Instantiate(tm, m, Quaternion.identity);
        }
        else
        {
            var wrong = (TextMesh)Instantiate(tmw, m, Quaternion.identity);
        }
    }
}