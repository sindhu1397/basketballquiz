﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.EventSystems;

public class Shooter : MonoBehaviour,IPointerEnterHandler,IPointerExitHandler {

    public Rigidbody prefab;
    public float speed =5f;
    public Vector3 f;
    DateTime t;
    public TextMesh tm;
    public Vector3 m;
    private float timer;
    float gazetime = 3f;
    private bool enter = false;
    
   
    
	public void Shoot () {
        Invoke("Hoop", 5);
        //Invoke("Notice", 9);
        /*if (enter)
        {
            timer += Time.deltaTime;

            if (timer > gazetime)
            {
                Invoke("Hoop", 1);
            }
        }*/
	}
	
	public void Hoop () {



        if ((DateTime.Now - t).TotalSeconds > 5)
        {
            var ball = (Rigidbody)Instantiate(prefab, f, Quaternion.identity);
            ball.velocity = (transform.up -transform.forward*1f) * speed;
            t = DateTime.Now;

        }
	}
   /*public void Notice()
    {
        var notice = (TextMesh)Instantiate(tm, m, Quaternion.identity);

    }*/

    public void OnPointerExit(PointerEventData eventData)
    {
        enter = false;
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        enter = true;
    }
}
