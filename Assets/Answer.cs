﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Answer : MonoBehaviour {
    private Question currentQuestion;
    public TextMesh tm;
    public TextMesh tmw;
    public Vector3 m;
    public void hoop1()
    {
        if (currentQuestion.op1 == currentQuestion.answer)
        {
            Debug.Log("Right");
        }
        else
        {
            Debug.Log("Wrong");
        }
    }
}
