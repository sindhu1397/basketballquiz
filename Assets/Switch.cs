﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Switch : MonoBehaviour {
    
	public void Startgame () {
        Invoke("Loader", 4);
	}
	public void Loader()
    {
        SceneManager.LoadScene("Scenes/game");
    }
 
     public void Instructions()
    {
        Invoke("Load", 4);
    }
    public void Load()
    {
        SceneManager.LoadScene("Scenes/instructions");
    }
    public void back()
    {
        Invoke("first", 4);
    }
    public void first()
    {
        SceneManager.LoadScene("Scenes/Start");
    }

}

